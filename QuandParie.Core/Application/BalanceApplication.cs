﻿using QuandParie.Core.Gateway;
using QuandParie.Core.Gateway.Contracts;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Application
{
    public class BalanceApplication
    {
        private readonly ICustomerRepository repository;
        private readonly IBankingSystem bankingSystem;

        public BalanceApplication(ICustomerRepository repository, IBankingSystem bankingSystem)
        {
            this.repository = repository;
            this.bankingSystem = bankingSystem;
        }

        public async Task ReceivePayment(string email, string paymentToken)
        {
            var customer = await repository.GetAsync(email)
                ?? throw new ArgumentException($"User {email} not found", nameof(email));

            var transactionInformation = await bankingSystem.Receive(paymentToken)
                ?? throw new ArgumentException($"Invalid payment token {paymentToken}", nameof(paymentToken));

            customer.Credit(transactionInformation.Amount);
        }

        public async Task EmitTransfer(string email, string iban, decimal amount)
        {
            var customer = await repository.GetAsync(email)
                ?? throw new ArgumentException($"User {email} not found", nameof(email));

            customer.Debit(amount);
            await bankingSystem.Transfer(new WireTransferData(amount, iban));
        }
    }
}
